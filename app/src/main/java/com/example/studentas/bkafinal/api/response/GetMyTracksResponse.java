package com.example.studentas.bkafinal.api.response;



import com.example.studentas.bkafinal.api.model.Item;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by studentas on 15.7.2.
 */
public class GetMyTracksResponse {

    public List<Item> getItems() {
        return items;
    }

    @SerializedName("items")
    private List<Item> items;
}
