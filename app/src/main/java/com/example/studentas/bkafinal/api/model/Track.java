package com.example.studentas.bkafinal.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by studentas on 15.7.2.
 */
public class Track {

    @SerializedName("album")
    private Album album;

    public List<Artist> getArtists() {
        return artists;
    }

    public String getDuration() {
        return duration;
    }

    @SerializedName("duration_ms")
    private String duration;

    @SerializedName("artists")
    private List<Artist> artists;


    public Album getAlbum() {
        return album;
    }
}
