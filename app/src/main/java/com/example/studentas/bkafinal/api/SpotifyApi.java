package com.example.studentas.bkafinal.api;

import com.example.studentas.bkafinal.api.response.GetAlbumResponse;
import com.example.studentas.bkafinal.api.response.GetMyTracksResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by studentas on 15.7.2.
 */
public interface SpotifyApi {

    @GET("/v1/me/tracks")
    void getMyTracks(Callback<GetMyTracksResponse> callback);

    @GET("/v1/albums/{id}")
    void getAlbum(@Path("id") String id, Callback<GetAlbumResponse> callback);
}
