package com.example.studentas.bkafinal.dagger;

import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import javax.inject.Scope;

/**
 * Created by studentas on 15.7.3.
 */
@Scope
@Retention(RUNTIME)
public @interface PerApplicationScope {

}
