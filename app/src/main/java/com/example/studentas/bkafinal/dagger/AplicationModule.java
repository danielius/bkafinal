package com.example.studentas.bkafinal.dagger;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.studentas.bkafinal.BkaApplication;
import com.example.studentas.bkafinal.MainActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by studentas on 15.7.3.
 */
@Module
@PerApplicationScope
public class AplicationModule {

    private BkaApplication bkaApplication;

    public AplicationModule(BkaApplication bkaApplication) {
        this.bkaApplication = bkaApplication;
    }

    @Provides
    @PerApplicationScope
    SharedPreferences preferences() {
        return bkaApplication.getSharedPreferences(MainActivity.PREF_PLAYER, Context.MODE_PRIVATE);
    }
}
