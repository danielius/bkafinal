package com.example.studentas.bkafinal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.IconButton;

import com.example.studentas.bkafinal.dagger.ApplicationComponent;
import com.example.studentas.bkafinal.dagger.PlayerComponent;
import com.example.studentas.bkafinal.dagger.PlayerModule;
import com.example.studentas.bkafinal.fragments.TracksFragment;
import com.joanzapata.android.iconify.Iconify;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.Spotify;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements
        ConnectionStateCallback {

    private static final String CLIENT_ID = "8a91678afa49446c9aff1beaabe9c807";
    private static final String REDIRECT_URI = "testschema://callback";
    private static final int REQUEST_CODE = 1337;
    public static final String KEY_ACCESS_TOKEN = "token";
    public static final String PREF_PLAYER = "player";

    private Player mPlayer;
    PlayerState playerState;
    @Inject
    SharedPreferences preferences;

    private ApplicationComponent component;
    private PlayerComponent playerComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        component = ((BkaApplication) getApplicationContext()).getComponent();
        component.inject(this);
    }


    public void onResume() {
        super.onResume();

        if (mPlayer == null || !mPlayer.isLoggedIn()) {
            openLoginWindow();
        }
    }

    private void openLoginWindow() {
        final AuthenticationRequest request = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI).setScopes(new String[]{"user-read-private", "playlist-read", "playlist-read-private", "streaming"}).build();

        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        //check if the result came from the same activity
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);

            switch (response.getType()) {
                case TOKEN:
                    // success

                    onAuthenticationComplete(response);
                    break;
                case ERROR:
                    //error
                    break;
                default:
            }

        }
    }

    private void onAuthenticationComplete(AuthenticationResponse response) {
        String accessToken = response.getAccessToken();

        preferences.edit().putString(KEY_ACCESS_TOKEN, accessToken).apply();

        Config playerConfig = new Config(this, response.getAccessToken(), CLIENT_ID);
        if (mPlayer == null) {
            mPlayer = Spotify.getPlayer(playerConfig, this, new Player.InitializationObserver() {
                @Override
                public void onInitialized(Player player) {
                    mPlayer.addConnectionStateCallback(MainActivity.this);

                }

                @Override
                public void onError(Throwable throwable) {
                    Log.e("MainActivity", "Could not initialize player: " + throwable.getMessage());
                }
            });

            playerComponent = component.getPlayerComponent(new PlayerModule(mPlayer));
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, TracksFragment.newInstance())
                .addToBackStack(TracksFragment.class.getSimpleName())
                .commit();

    }

    @Override
    public void onLoggedIn() {
        Log.d("MainActivity", "User logged in");
    }

    @Override
    public void onLoggedOut() {
        Log.d("MainActivity", "User logged out");
    }

    @Override
    public void onLoginFailed(Throwable error) {
        Log.d("MainActivity", "Login failed");
    }

    @Override
    public void onTemporaryError() {
        Log.d("MainActivity", "Temporary error occurred");
    }

    @Override
    public void onConnectionMessage(String message) {
        Log.d("MainActivity", "Received connection message: " + message);
    }


    @Override
    protected void onDestroy() {
        //ButterKnife.unbind(this);
        Spotify.destroyPlayer(this);
        super.onDestroy();
    }

    public ApplicationComponent getComponent() {
        return component;
    }

    public PlayerComponent getPlayerComponent() {
        return playerComponent;
    }
}
