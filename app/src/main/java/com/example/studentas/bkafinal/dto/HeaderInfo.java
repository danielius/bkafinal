package com.example.studentas.bkafinal.dto;

import com.example.studentas.bkafinal.api.model.Artist;
import com.example.studentas.bkafinal.api.model.Image;

import java.util.List;

/**
 * Created by studentas on 15.7.3.
 */
public class HeaderInfo {
    List<Artist> artists;
    List<Image> images;
    String albumName;

    public HeaderInfo(List<Artist> artists, List<Image> images,String albumName) {
        this.artists = artists;
        this.images = images;
        this.albumName = albumName;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public Image getImage(){
        for(Image image: images){
            if(image.getHeight()>300){
                return image;
            }
        }
        return null;
    }

    public String getAlbumName() {
        return albumName;
    }
}
