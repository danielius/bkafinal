package com.example.studentas.bkafinal.api.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by studentas on 15.7.2.
 */
public class Album {

    @SerializedName("album_type")
    private String type;

    @SerializedName("name")
    private String albumName;

    @SerializedName("images")
    private List<Image> images;

    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public Image getMediumImage(){
        for(Image image: images){
            if(image.getHeight()<=300){
                return image;
            }
        }
        return null;
    }

    public String getAlbumName() {
        return albumName;
    }

}
