package com.example.studentas.bkafinal.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.IconButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.studentas.bkafinal.MainActivity;
import com.example.studentas.bkafinal.R;
import com.example.studentas.bkafinal.api.SpotifyApi;
import com.example.studentas.bkafinal.api.model.AlbumTrackItem;
import com.example.studentas.bkafinal.api.model.Artist;
import com.example.studentas.bkafinal.api.response.GetAlbumResponse;
import com.example.studentas.bkafinal.dto.HeaderInfo;
import com.joanzapata.android.iconify.Iconify;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class AlbumDetailsFragment extends Fragment implements PlayerNotificationCallback {

    @Inject
    SpotifyApi spotifyApi;
    @Inject
    Player mPlayer;

    @Bind(R.id.album_details_list)
    RecyclerView recycler;

    @Bind(R.id.current_time)
    TextView time;

    @Bind(R.id.btn_backward)
    IconButton backward;

    @Bind(R.id.btn_play)
    IconButton play;
    @Bind(R.id.btn_forward)
    IconButton forward;

    private MyRecyclerAdapter adapter;
    private List<HolderEntity> items = new ArrayList<HolderEntity>();
    private PlayerState playerState = new PlayerState();

    public static AlbumDetailsFragment newInstance(String albumId) {
        Bundle arguments = new Bundle();
        arguments.putString("id", albumId);

        AlbumDetailsFragment fragment = new AlbumDetailsFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_album_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MyRecyclerAdapter(items);
        recycler.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity) getActivity()).getPlayerComponent().inject(this);

        mPlayer.addPlayerNotificationCallback(this);

        spotifyApi.getAlbum(getArguments().getString("id"), new Callback<GetAlbumResponse>() {
            @Override
            public void success(GetAlbumResponse albumDetails, Response response) {
                Toast.makeText(getActivity(), "success", Toast.LENGTH_LONG).show();
                //Snackbar.make(getView(), "succcesss", Snackbar.LENGTH_SHORT);
                HolderEntity entity = new HolderEntity(new HeaderInfo(albumDetails.getArtists(),
                        albumDetails.getImages(), albumDetails.getAlbumName()));
                items.add(entity);

                for (AlbumTrackItem trackItem : albumDetails.getTracks().getItems()) {
                    items.add(new HolderEntity(trackItem));
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                //Snackbar.make(getView(), "fail", Snackbar.LENGTH_SHORT);
                Toast.makeText(getActivity(), "failure", Toast.LENGTH_LONG).show();
                Log.e("Error", error.getMessage(), error.getCause());
            }
        });
    }


    @Override
    public void onPlaybackEvent(PlayerNotificationCallback.EventType eventType, PlayerState playerState) {
        this.playerState = playerState;

        Log.d("MainActivity", "Playback event received: " + eventType.name());


        if (play != null) {
            if (playerState.playing) {
                play.setText("{fa-pause}");

            } else {
            mPlayer.pla
                play.setText("{fa-play}");
            }
            Iconify.addIcons(play);
        }
    }

    @OnClick(R.id.btn_play)
    void onClickPlay(){
        if (playerState.playing) {
            mPlayer.pause();

        } else if(playerState.trackUri!=null){
            mPlayer.resume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mPlayer!=null){
            mPlayer.pause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onPlaybackError(PlayerNotificationCallback.ErrorType errorType, String errorDetails) {
        Log.d("MainActivity", "Playback error received: " + errorType.name());
    }

    private class MyRecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {
        public static final int HEADER = 0;
        public static final int SONG = 1;
        private final List<HolderEntity> items;

        public MyRecyclerAdapter(List<HolderEntity> items) {
            this.items = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
            View view;
            switch (getItemViewType(position)) {
                case HEADER:
                    view = getActivity().getLayoutInflater().inflate(R.layout.row_album_header, parent, false);
                    return new HeaderHolder(view);
                case SONG:
                    view = getActivity().getLayoutInflater().inflate(R.layout.row_album_track, parent, false);
                    return new SongHolder(view);
                default:
                    throw new IllegalArgumentException("Invalid View Type");
            }
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            final HolderEntity entity = items.get(position);
            if (viewHolder instanceof HeaderHolder) {
                HeaderHolder holder = (HeaderHolder) viewHolder;

                if (entity.getHeader().getImage() != null) {
                    Picasso.with(holder.itemView.getContext())
                            .load(entity.getHeader().getImage().getUrl())
                            .placeholder(R.drawable.images)
                            .into(holder.headerPhoto);
                }
                String authors = "";
                for (Artist artist : entity.getHeader().getArtists()) {
                    authors = authors.concat(artist.getName() + " ");
                }
                holder.albumName.setText(entity.getHeader().getAlbumName());
                holder.albumAuthor.setText(authors);



            } else {
                SongHolder holder = (SongHolder) viewHolder;

                String authors = "";
                for (Artist artist : entity.getSong().getArtists()) {
                    authors = authors.concat(artist.getName() + ",");
                }
                holder.trackAuthor.setText(authors);
                holder.trackTitle.setText(entity.getSong().getName());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mPlayer!=null){
                            mPlayer.play(entity.getSong().getUri());

                        }
                    }
                });
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return HEADER;
            } else {
                return SONG;
            }

        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    class HeaderHolder extends ViewHolder {

        @Bind(R.id.header_photo)
        ImageView headerPhoto;

        @Bind(R.id.album_name)
        TextView albumName;

        @Bind(R.id.album_author)
        TextView albumAuthor;

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }

    class SongHolder extends ViewHolder {

        @Bind(R.id.album_track_author)
        TextView trackAuthor;

        @Bind(R.id.album_track_title)
        TextView trackTitle;

        public SongHolder(View itemView) {
            super(itemView);
        }
    }
}
