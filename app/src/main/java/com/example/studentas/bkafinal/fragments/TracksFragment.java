package com.example.studentas.bkafinal.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.studentas.bkafinal.MainActivity;
import com.example.studentas.bkafinal.R;
import com.example.studentas.bkafinal.api.SpotifyApi;
import com.example.studentas.bkafinal.api.model.Item;
import com.example.studentas.bkafinal.api.response.GetMyTracksResponse;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;


/**
 * A placeholder fragment containing a simple view.
 */
public class TracksFragment extends Fragment {

    @Inject
    SharedPreferences preferences;

    @Inject
    SpotifyApi spotifyApi;

    List<Item> items = new ArrayList<Item>();

    @Bind(R.id.track_list)
    RecyclerView recycler;
    private MyRecyclerAdapter adapter;

    public static TracksFragment newInstance() {
        Bundle arguments = new Bundle();
        arguments.putString("username", "user");

        TracksFragment fragment = new TracksFragment();
        fragment.setArguments(arguments);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        int widthPixels = metrics.widthPixels;
        int padding = getResources().getDimensionPixelSize(R.dimen.padding_small);
        int cardWidth = (widthPixels - 4 * padding) / 2;

        recycler.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapter = new MyRecyclerAdapter(items, cardWidth);
        recycler.setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tracks, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((MainActivity) getActivity()).getComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        spotifyApi.getMyTracks(new Callback<GetMyTracksResponse>() {
            @Override
            public void success(GetMyTracksResponse tracks, Response response) {
                Toast.makeText(getActivity(), "success", Toast.LENGTH_LONG).show();
                //Snackbar.make(getView(), "succcesss", Snackbar.LENGTH_SHORT);

                items.addAll(tracks.getItems());
                adapter.notifyDataSetChanged();
                ;
            }

            @Override
            public void failure(RetrofitError error) {
                //Snackbar.make(getView(), "fail", Snackbar.LENGTH_SHORT);
                Toast.makeText(getActivity(), "success", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private class MyRecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {
        private final List<Item> items;
        private int cardWidth;

        public MyRecyclerAdapter(List<Item> items, int cardWidth) {
            this.items = items;
            this.cardWidth = cardWidth;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
            View view = getActivity().getLayoutInflater().inflate(R.layout.row_track, parent, false);

            return new ViewHolder(view, cardWidth);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            final Item item = items.get(position);

            viewHolder.author.setText(item.getTrack().getArtists().get(0).getName());
            viewHolder.title.setText(item.getTrack().getAlbum().getAlbumName());
            viewHolder.duration.setText(item.getTrack().getDuration());

            if (item.getTrack().getAlbum().getMediumImage() != null) {
                Picasso.with(viewHolder.itemView.getContext())
                        .load(item.getTrack().getAlbum().getMediumImage().getUrl())
                        .placeholder(R.drawable.images)
                        .into(viewHolder.photo);
            }

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment, AlbumDetailsFragment.newInstance(item.getTrack().getAlbum().getId()))
                            .addToBackStack(AlbumDetailsFragment.class.getSimpleName())
                            .commit();
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.track_photo)
        ImageView photo;

        @Bind(R.id.track_title)
        TextView title;

        @Bind(R.id.track_author)
        TextView author;

        @Bind(R.id.track_duration)
        TextView duration;

        public ViewHolder(View itemView, int cardWidth) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            photo.setMaxHeight(cardWidth);
        }

    }

}
