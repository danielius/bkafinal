package com.example.studentas.bkafinal.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by studentas on 15.7.3.
 */
public class AlbumTracks {

    @SerializedName("items")
    private List<AlbumTrackItem> items;

    public List<AlbumTrackItem> getItems() {
        return items;
    }
}
