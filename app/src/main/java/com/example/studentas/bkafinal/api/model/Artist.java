package com.example.studentas.bkafinal.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by studentas on 15.7.2.
 */
public class Artist {
    public String getName() {
        return name;
    }

    @SerializedName("name")
    private String name;
}
