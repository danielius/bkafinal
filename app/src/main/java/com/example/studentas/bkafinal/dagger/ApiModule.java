package com.example.studentas.bkafinal.dagger;

import android.content.SharedPreferences;

import com.example.studentas.bkafinal.MainActivity;
import com.example.studentas.bkafinal.api.SpotifyApi;
import com.google.gson.Gson;

import javax.inject.Scope;

import dagger.Module;
import dagger.Provides;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by studentas on 15.7.3.
 */
@Module
@PerApplicationScope
public class ApiModule {

    @Provides
    @PerApplicationScope
    SpotifyApi spotify(RequestInterceptor interseptor){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.spotify.com")
                .setConverter(new GsonConverter(new Gson()))
                .setRequestInterceptor(interseptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return restAdapter.create(SpotifyApi.class);
    }

    @Provides
    @PerApplicationScope
    RequestInterceptor interseptor(final SharedPreferences preferences){
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization", "Bearer " + preferences.getString(MainActivity.KEY_ACCESS_TOKEN, ""));
            }
        };
    }
}
