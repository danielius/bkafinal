package com.example.studentas.bkafinal.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by studentas on 15.7.2.
 */
public class Item {
    @SerializedName("added_at")
    private String addedTimeStamp;

    @SerializedName("track")
    private Track track;

    public String getAddedTimeStamp() {
        return addedTimeStamp;
    }

    public Track getTrack() {
        return track;
    }
}
