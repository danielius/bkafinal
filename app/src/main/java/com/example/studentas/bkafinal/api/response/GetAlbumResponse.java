package com.example.studentas.bkafinal.api.response;

import com.example.studentas.bkafinal.api.model.AlbumTracks;
import com.example.studentas.bkafinal.api.model.Artist;
import com.example.studentas.bkafinal.api.model.Image;
import com.example.studentas.bkafinal.api.model.Item;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by studentas on 15.7.3.
 */
public class GetAlbumResponse {

    @SerializedName("artists")
    private List<Artist> artists;

    @SerializedName("tracks")
    private AlbumTracks tracks;

    @SerializedName("images")
    private List<Image> images;

    @SerializedName("name")
    private String albumName;

    public List<Artist> getArtists() {
        return artists;
    }

    public List<Image> getImages() {
        return images;
    }

    public String getAlbumName() {
        return albumName;
    }

    public AlbumTracks getTracks() {
        return tracks;
    }
}
