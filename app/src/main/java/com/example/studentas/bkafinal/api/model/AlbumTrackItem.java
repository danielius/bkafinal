package com.example.studentas.bkafinal.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by studentas on 15.7.3.
 */
public class AlbumTrackItem {

    @SerializedName("name")
    private String name;

    @SerializedName("uri")
    private String uri;

    @SerializedName("duration_ms")
    private int durationMs;

    @SerializedName("artists")
    private List<Artist> artists;

    public String getName() {
        return name;
    }

    public String getUri() {
        return uri;
    }

    public int getDurationMs() {
        return durationMs;
    }

    public List<Artist> getArtists() {
        return artists;
    }
}
