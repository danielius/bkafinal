package com.example.studentas.bkafinal;

import android.app.Application;

import com.example.studentas.bkafinal.dagger.ApiModule;
import com.example.studentas.bkafinal.dagger.AplicationModule;
import com.example.studentas.bkafinal.dagger.ApplicationComponent;
import com.example.studentas.bkafinal.dagger.DaggerApplicationComponent;

/**
 * Created by studentas on 15.7.3.
 */
public class BkaApplication extends Application {
    ApplicationComponent component;
    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder().apiModule(new ApiModule()).aplicationModule(new AplicationModule(this)).build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
