package com.example.studentas.bkafinal.dagger;

import com.spotify.sdk.android.player.Player;

import dagger.Module;
import dagger.Provides;

/**
 * Created by studentas on 15.7.3.
 */
@Module
@PerApplicationScope
public class PlayerModule {

    private Player mPlayer;

    public PlayerModule(Player mPlayer) {

        this.mPlayer = mPlayer;
    }

    @Provides
    Player mPlayer(){
        return mPlayer;
    }
}
