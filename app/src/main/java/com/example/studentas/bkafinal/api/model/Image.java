package com.example.studentas.bkafinal.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by studentas on 15.7.2.
 */
public class Image {

    @SerializedName("height")
    private int height;

    @SerializedName("width")
    private int width;

    @SerializedName("url")
    private String url;

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public String getUrl() {
        return url;
    }
}
