package com.example.studentas.bkafinal.fragments;

import com.example.studentas.bkafinal.api.model.AlbumTrackItem;
import com.example.studentas.bkafinal.dto.HeaderInfo;

/**
 * Created by studentas on 15.7.3.
 */
public class HolderEntity {
    HeaderInfo header;
    AlbumTrackItem song;

    public HolderEntity(HeaderInfo header) {
        this.header = header;
    }

    public HolderEntity(AlbumTrackItem song) {
        this.song = song;
    }

    public HeaderInfo getHeader() {
        return header;
    }

    public AlbumTrackItem getSong() {
        return song;
    }
}