package com.example.studentas.bkafinal.dagger;

import com.example.studentas.bkafinal.MainActivity;
import com.example.studentas.bkafinal.fragments.AlbumDetailsFragment;
import com.example.studentas.bkafinal.fragments.TracksFragment;

import dagger.Component;
import dagger.Module;

/**
 * Created by studentas on 15.7.3.
 */
@PerApplicationScope
@Component(
        modules = {ApiModule.class,AplicationModule.class}
)
public interface ApplicationComponent {

    void inject(MainActivity activity);

    void inject(TracksFragment tracksFragment);

    PlayerComponent getPlayerComponent(PlayerModule playerModule);
}
