package com.example.studentas.bkafinal.dagger;

import com.example.studentas.bkafinal.fragments.AlbumDetailsFragment;

import dagger.Subcomponent;

/**
 * Created by studentas on 15.7.3.
 */
@Subcomponent(modules = PlayerModule.class)
public interface PlayerComponent {

    void inject(AlbumDetailsFragment albumDetailsFragment);
}
